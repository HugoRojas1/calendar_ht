import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { Switch } from 'react-router'
import Home from '../../pages'

const Routes = () => (
  <Router basename={process.env.REACT_APP_SUB_DIRECTORY}>
    <Switch>
      <Route exact path='/' component={Home} />
    </Switch>
  </Router>
)

export default Routes
