import styled, { css } from 'styled-components'

export const Cell = styled.div`
  position: relative;
  width: calc(100% / 7);
  height: 16vh;
  /* display: flex;
  align-items: center;
  justify-content: center; */
  cursor: pointer;
  background: ${(props) => (props.isWeekend ? '#eeeeee' : 'inherit')};
  color: ${(props) => (props.isWeekend ? '#2e74b2' : 'inherit')};

  /* &.date-cell {
    border: solid gray 1px;
  } */

  ${(props) =>
    props.isToday &&
    css`
      background: #ffffe3;
    `}

  ${(props) =>
    props.isSelected &&
    css`
      background-color: #fffde6;
    `}
`

export const DateNumber = styled.label`
  padding: 5px;
  font-weight: 600;
  font-size: 14px;
`

export const CellWrapper = styled.div`
  border: solid #d8d8d8 1px;
  height: 100%;
`

export const Reminder = styled.div`
  height: 3vh;
  background: ${(props) => props.secondary};
  color: ${(props) => props.primary};
  border-left: solid 5px ${(props) => props.primary};
  padding: 0 5px;
  margin-top: 10px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`
export const MoreReminders = styled.div`
  margin-top: 5px;
  font-size: 13px;
  color: #2e74b2;
  font-weight: bold;
`
