import * as React from 'react'
import { useEffect } from 'react'
import { Cell, CellWrapper, DateNumber, Reminder, MoreReminders } from './style'
const today = new Date()

const CellBox = ({ reminders, currentDate, handleClickOnCelll }) => {
  useEffect(() => {}, [])

  const cellDate = new Date(
    currentDate.year,
    currentDate.month,
    currentDate.dayNumber
  )
  return (
    <Cell
      isToday={cellDate === today.getDate()}
      isSelected={currentDate.dayNumber === currentDate.day}
      isWeekend={cellDate.getDay() % 6 === 0}
      onClick={() => handleClickOnCelll(currentDate.dayNumber)}>
      <CellWrapper>
        <DateNumber>
          {currentDate.dayNumber > 0 ? currentDate.dayNumber : ''}
        </DateNumber>
        {reminders.length > 3 ? (
          <>
            {reminders.slice(0, 2).map((reminder, index) => {
              return (
                <Reminder
                  key={index}
                  primary={reminder.primary}
                  secondary={reminder.secondary}>
                  {reminder.name}
                </Reminder>
              )
            })}
            <MoreReminders> {reminders.length - 2} more...</MoreReminders>
          </>
        ) : (
          <>
            {reminders.map((reminder, index) => {
              return (
                <Reminder
                  key={index}
                  primary={reminder.primary}
                  secondary={reminder.secondary}>
                  {reminder.name}
                </Reminder>
              )
            })}
          </>
        )}
      </CellWrapper>
    </Cell>
  )
}

export default CellBox
