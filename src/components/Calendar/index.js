import * as React from 'react'
import { useState, useEffect, useContext } from 'react'
import Cell from './components/Cell'
import ReminderContext from '../../context/ReminderContext'
import Modal from 'react-modal'
import { getWeatherByCity } from '../../service/weather'
import {
  Wrapper,
  Header,
  Button,
  Body,
  CellHeader,
  ChevronNext,
  HeaderReminder,
  BodyRemainder,
  ReeminderSection,
  ReminderDate,
  ActionsWrapper,
  EventInfo,
  EventName,
  EventActions,
  ActionButton,
  Field,
  Label,
  Input,
  FooterRemainder,
  DeleteReminders,
  Weather,
  ErrorMessage,
} from './style'
import {
  DAYS_LABEL,
  DAYS_LEAP,
  DAYS_OF_THE_WEEK,
  MONTHS_LABEL,
  modalStyles,
} from '../../constants'
const today = new Date()

const Calendar = () => {
  const {
    getReminders,
    addReminder,
    editReminder,
    deleteReminder,
    deleteAllReminders,
  } = useContext(ReminderContext)

  const getStartDayOfMonth = (date) => {
    return new Date(date.getFullYear(), date.getMonth(), 1).getDay()
  }

  const isLeapYear = (year) => {
    return (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0
  }

  const [reminders, setReminders] = useState([])

  const [date, setDate] = useState(today)
  const [day, setDay] = useState(date.getDate())
  const [month, setMonth] = useState(date.getMonth())
  const [year, setYear] = useState(date.getFullYear())
  const [startDay, setStartDay] = useState(getStartDayOfMonth(date))
  const [openModal, setOpenModal] = useState(false)
  const [actionModal, setActionModal] = useState('show')
  const [currentEvent, setCurrentEvent] = useState({
    name: '',
    city: '',
    tempCity: '',
    date: '',
    primary: '#2e74b2',
    secondary: '#9fbfe0',
  })
  const [weather, setWeather] = useState('')
  const [reminderError, setReminderError] = useState('')

  useEffect(() => {
    setDay(date.getDate())
    setMonth(date.getMonth())
    setYear(date.getFullYear())
    setStartDay(getStartDayOfMonth(date))
  }, [date])

  const fetchReminders = async () => {
    const dataRemainders = await getReminders()
    setReminders(dataRemainders)
  }

  useEffect(() => {
    fetchReminders()
  }, [reminders])

  const resetStates = () => {
    setActionModal('show')
    setOpenModal(false)
    setReminders([])
    setReminderError('')
  }

  const handleClickOnCelll = (dayNumber) => {
    setOpenModal(true)
    setActionModal('show')
    setDate(new Date(year, month, dayNumber))
  }

  const handleInputChange = (e) => {
    const event = Object.assign({}, currentEvent)
    event[e.target.name] = e.target.value
    setCurrentEvent(event)
  }

  const handleNewReminder = () => {
    setCurrentEvent({
      name: '',
      city: '',
      tempCity: '',
      date: '',
      primary: '#2e74b2',
      secondary: '#9fbfe0',
    })
    setActionModal('create')
    setWeather('')
    setReminderError('')
  }
  const addNewReminder = async () => {
    const response = await addReminder({
      ...currentEvent,
      city: currentEvent.tempCity,
      id: reminders.length + 1,
    })

    if (response && response.status === 'error') {
      setReminderError(response.message)
    } else {
      resetStates()
    }
  }

  const editReminderAction = async () => {
    const response = await editReminder({
      ...currentEvent,
      city: currentEvent.tempCity,
    })

    if (response && response.status === 'error') {
      setReminderError(response.message)
    } else {
      resetStates()
    }
  }

  const getWeather = async (city) => {
    const response = await getWeatherByCity(city)

    if (response.status === 'ok') {
      setWeather(response.data)
    }
  }

  const handleEditReminder = (reminder) => {
    reminder.tempCity = reminder.city
    setCurrentEvent(reminder)
    setActionModal('edit')
    getWeather(reminder.city)
  }

  const handleDeleteReminder = (id) => {
    deleteReminder(id)
    resetStates()
  }

  const handledeleteAllReminders = () => {
    deleteAllReminders()
    setReminders([])
  }

  const days = isLeapYear ? DAYS_LEAP : DAYS_LABEL
  return (
    <>
      <DeleteReminders onClick={handledeleteAllReminders}>
        Delete all reminders
      </DeleteReminders>
      <Wrapper>
        <Header>
          <Button onClick={() => setDate(new Date(year, month - 1, day))}>
            <ChevronNext className='rotate'></ChevronNext>
          </Button>
          <div>
            {MONTHS_LABEL[month]} {year}
          </div>
          <Button onClick={() => setDate(new Date(year, month + 1, day))}>
            <ChevronNext></ChevronNext>
          </Button>
        </Header>
        <Body>
          {DAYS_OF_THE_WEEK.map((day) => (
            <CellHeader key={day}>
              <strong>{day}</strong>
            </CellHeader>
          ))}
          {Array(days[month] + (startDay - 1))
            .fill(null)
            .map((_, index) => {
              const d = index - (startDay - 1)
              const dayReminders = reminders.filter((reminder) => {
                const cellDay = new Date(year, month, d)
                return (
                  reminder.date.split('T')[0] ===
                  cellDay.toISOString().split('T')[0]
                )
              })
              return (
                <Cell
                  key={index}
                  reminders={dayReminders}
                  currentDate={{ day, month, year, dayNumber: d }}
                  handleClickOnCelll={handleClickOnCelll}></Cell>
              )
            })}
        </Body>
        <Modal
          ariaHideApp={false}
          isOpen={openModal}
          onRequestClose={() => setOpenModal(false)}
          style={modalStyles}>
          <HeaderReminder>
            <ReeminderSection>
              <ReminderDate>
                {MONTHS_LABEL[month]} {day}
              </ReminderDate>
            </ReeminderSection>
            <ReeminderSection>
              <ActionsWrapper onClick={handleNewReminder}>
                Add Reminder
              </ActionsWrapper>
            </ReeminderSection>
          </HeaderReminder>

          {actionModal === 'show' && (
            <BodyRemainder>
              {reminders
                .filter((reminder) => {
                  const cellDay = new Date(year, month, day)
                  return (
                    reminder.date.split('T')[0] ===
                    cellDay.toISOString().split('T')[0]
                  )
                })
                .map((reminder, index) => {
                  return (
                    <EventInfo background={reminder.secondary} key={index}>
                      <EventName color={reminder.primary}>
                        {reminder.name}
                      </EventName>
                      <EventActions>
                        <ActionButton
                          onClick={() => handleEditReminder(reminder)}>
                          <strong>Edit</strong>
                        </ActionButton>
                        <ActionButton
                          onClick={() => handleDeleteReminder(reminder.id)}>
                          <strong>Delete</strong>
                        </ActionButton>
                      </EventActions>
                    </EventInfo>
                  )
                })}
            </BodyRemainder>
          )}

          {actionModal !== 'show' && (
            <BodyRemainder>
              {weather && (
                <Weather>
                  Weather in {currentEvent.city}: {weather}
                </Weather>
              )}
              <Field>
                <p>
                  <Label>Name:</Label>
                  <Input
                    autoComplete='off'
                    type='text'
                    name='name'
                    value={currentEvent.name}
                    max='30'
                    maxLength='30'
                    placeholder='Add reminder name'
                    onChange={handleInputChange}></Input>
                </p>
              </Field>
              <Field>
                <p>
                  <Label>City:</Label>
                  <Input
                    autoComplete='off'
                    type='text'
                    name='tempCity'
                    value={currentEvent.tempCity}
                    placeholder='Add reminder city'
                    onChange={handleInputChange}></Input>
                </p>
              </Field>
              <Field>
                <p>
                  <Label>Date</Label>
                  <Input
                    value={currentEvent.date}
                    autoComplete='off'
                    name='date'
                    type='datetime-local'
                    onChange={handleInputChange}></Input>
                </p>
              </Field>
              <Field>
                <div>
                  <Label>Primario</Label>
                  <Input
                    width={'30%'}
                    value={currentEvent.primary}
                    name='primary'
                    type='color'
                    onChange={handleInputChange}></Input>
                  <br></br>
                  <Label>Secundario</Label>
                  <Input
                    width={'30%'}
                    value={currentEvent.secondary}
                    name='secondary'
                    type='color'
                    onChange={handleInputChange}></Input>
                </div>
              </Field>
              <FooterRemainder>
                {actionModal === 'create' ? (
                  <ActionButton
                    disabled={
                      currentEvent.name === '' ||
                      currentEvent.tempCity === '' ||
                      currentEvent.date === ''
                    }
                    width={'120px'}
                    onClick={addNewReminder}>
                    Save
                  </ActionButton>
                ) : (
                  <ActionButton
                    disabled={
                      currentEvent.name === '' ||
                      currentEvent.tempCity === '' ||
                      currentEvent.date === ''
                    }
                    width={'120px'}
                    onClick={editReminderAction}>
                    Update
                  </ActionButton>
                )}
                <ErrorMessage>{reminderError} </ErrorMessage>
              </FooterRemainder>
            </BodyRemainder>
          )}
        </Modal>
      </Wrapper>
    </>
  )
}

export default Calendar
