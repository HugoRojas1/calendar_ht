import styled from 'styled-components'

export const Wrapper = styled.div`
  width: 90%;
  margin: auto;
  border: 1px solid lightgrey;
  box-shadow: 2px 2px 2px #eee;
`

export const Header = styled.div`
  font-size: 18px;
  margin: 10px;
  font-weight: bold;
  padding: 10px 10px 5px 10px;
  display: flex;
  color: #2e74b2;
  justify-content: space-between;
  background-color: #ffffff;
`

export const Button = styled.div`
  cursor: pointer;
`

export const Body = styled.div`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
`

export const CellHeader = styled.div`
  position: relative;
  width: calc(100% / 7);
  height: auto;
  display: flex;
  padding: 10px 0;
  align-items: center;
  justify-content: center;
  cursor: pointer;
  background: #2e74b2;
  color: #fff;
`

export const ChevronNext = styled.div`
  position: relative;
  padding: 0px;
  height: 17px;
  width: 6px;
  margin-left: 0px;
  margin-top: 0px;

  &.rotate {
    -webkit-transform: rotate(180deg);
    -moz-transform: rotate(180deg);
    -ms-transform: rotate(180deg);
    -o-transform: rotate(180deg);
    transform: rotate(180deg);
  }

  &::before {
    content: '';
    position: absolute;
    top: 0;
    left: 0;
    height: 50%;
    width: 100%;
    background: #2e74b2;
    -webkit-transform: skew(25deg, 0deg);
    -moz-transform: skew(25deg, 0deg);
    -ms-transform: skew(25deg, 0deg);
    -o-transform: skew(25deg, 0deg);
    transform: skew(25deg, 0deg);
  }

  &::after {
    content: '';
    position: absolute;
    top: 50%;
    right: 0;
    height: 50%;
    width: 100%;
    background: #2e74b2;
    -webkit-transform: skew(-25deg, 0deg);
    -moz-transform: skew(-25deg, 0deg);
    -ms-transform: skew(-25deg, 0deg);
    -o-transform: skew(-25deg, 0deg);
    transform: skew(-25deg, 0deg);
  }
`
// export const ExitIcon = styled.div`
//   color: #2e74b2;
//   font-size: 30px;
//   position: absolute;
//   top: 0;
//   right: 15px;
//   font-weight: bold;
//   cursor: pointer;
// `

export const HeaderReminder = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
`

export const ReeminderSection = styled.div`
  width: 50%;
`

export const ReminderDate = styled.div`
  padding: 15px 0;
  background: #2e74b2;
  color: #fff;
  font-weight: bolder;
  text-transform: uppercase;
  letter-spacing: 1px;
`

export const ActionsWrapper = styled.div`
  padding: 15px 0;
  background: #e3f1ff;
  border-left: 15px solid #6aa3d6;
  color: #2e74b2;
  font-weight: bolder;
  text-decoration: underline;
  letter-spacing: 1px;
  cursor: pointer;
`

export const BodyRemainder = styled.div`
  overflow: auto;
  height: 250px;
  margin-top: 10px;
`

export const EventInfo = styled.div`
  display: flex;
  padding: 10px;
  margin: 15px 0;
  background: ${(props) => (props.background ? props.background : '#e3f1ff')};
`

export const EventName = styled.p`
  width: 70%;
  text-align: left;
  margin: 0;
  color: ${(props) => (props.color ? props.color : '#2e74b2')};
`

export const EventActions = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  width: 30%;
`
export const ActionButton = styled.button`
  cursor: pointer;
  padding: 5px 10px;
  border-radius: 4px;
  border: none;
  background: #2e74b2;
  color: #ffffff;
  width: ${(props) => (props.width ? props.width : 'auto')};

  &:disabled {
    background: gray !important;
    cursor: not-allowed;
  }
`

export const Field = styled.div`
  text-align: left;
  width: 100%;
`

export const Label = styled.label`
  justify-content: flex-end;
  display: inline-flex;
  width: 100px;
  margin-right: 15px;
`

export const Input = styled.input`
  width: ${(props) => (props.width ? props.width : '60%')};
  border-width: 0 0 1px 0;
  border-color: gray;
  border-style: solid;
  outline: none;
  margin-top: 5px;
`

export const FooterRemainder = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  text-align: right;
  margin-top: 15px;
  margin-bottom: 30px;
`
export const DeleteReminders = styled.p`
  width: 95%;
  text-align: right;
  color: #2e74b2;
  font-size: 12px;
  font-weight: bold;
  cursor: pointer;
`

export const Weather = styled.span`
  position: absolute;
  right: 25px;
  font-size: 12px;
`

export const ErrorMessage = styled.span`
  color: red;
  font-size: 13px;
`
