import React, { useState } from 'react'
import ReactDOM from 'react-dom'
import registerServiceWorker from './registerServiceWorker'
import Routes from './components/Routes'
import { get, add, update, remove, removeAll } from './service/reminders'
import ReminderContext from './context/ReminderContext'

const App = () => {
  const [reminders, setReminders] = useState([])

  const addReminder = async (reminder) => {
    const addedReminder = await add(reminder)
    if (addedReminder.status === 'ok') {
      getReminders()
    } else {
      return addedReminder
    }
  }

  const editReminder = async (reminder) => {
    const editReminder = await update(reminder)
    if (editReminder.status === 'ok') {
      getReminders()
    } else {
      return editReminder
    }
  }

  const deleteReminder = (id) => {
    remove(id)
    getReminders()
  }

  const deleteAllReminders = async () => {
    const deleteAllReminders = await removeAll()
    if (deleteAllReminders) {
      getReminders()
    }
  }

  const getReminders = async () => {
    const remaindersData = await get()
    setReminders(remaindersData)
    return remaindersData
  }

  return (
    <ReminderContext.Provider
      value={{
        reminders,
        addReminder,
        editReminder,
        getReminders,
        deleteReminder,
        deleteAllReminders,
      }}>
      <Routes />
    </ReminderContext.Provider>
  )
}

ReactDOM.render(<App />, document.getElementById('root'))
registerServiceWorker()
