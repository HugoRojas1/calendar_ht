export const findDuplicity = (dataArr, itemArr) => {
  const foundedDuplicityItem = dataArr.find(
    (dataItem) => dataItem.name === itemArr.name
  )

  if (foundedDuplicityItem && foundedDuplicityItem.id === itemArr.id) {
    //Alow to edit the same reminder where found the duplicity
    return false
  }

  return !!foundedDuplicityItem
}
