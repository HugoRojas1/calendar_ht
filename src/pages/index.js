import React from 'react'
import Calendar from '../components/Calendar'
import '../assets/css/styles.css'

const Home = () => {
  return (
    <div className='container'>
      <Calendar></Calendar>
    </div>
  )
}

export default Home
