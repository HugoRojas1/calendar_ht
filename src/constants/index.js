export const DAYS_LABEL = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
export const DAYS_LEAP = [31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]
export const DAYS_OF_THE_WEEK = [
  'Sunday',
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
]
export const MONTHS_LABEL = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
]

export const modalStyles = {
  overlay: {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    overflow: 'auto',
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    zIndex: 100,
  },
  content: {
    position: 'relative',
    WebkitOverflowScrolling: 'touch',
    outline: 'none',
    padding: '0',
    border: 'none',
    overflow: 'hidden',
    maxWidth: '500px',
    minHeight: '300px',
    maxHeight: '350px',
    width: '100%',
    textAlign: 'center',
    zIndex: 100,
    top: '30%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    transform: 'translate(-50%, -50%)',
  },
}
