import { REMINDERS } from './data'
import { findDuplicity } from '../helpers/utils'

export const get = () => {
  return REMINDERS
}

export const add = (r) => {
  const duplicateReminder = findDuplicity(REMINDERS, r)

  if (duplicateReminder) {
    return { status: 'error', message: 'Duplicate reminder name' }
  } else {
    const size = REMINDERS.length
    REMINDERS.push({ ...r, id: REMINDERS[size - 1].id + 1 })
    return { status: 'ok' }
  }
}

export const update = (r) => {
  const editedReminderIndex = REMINDERS.findIndex(
    (reminder) => reminder.id === r.id
  )

  if (editedReminderIndex >= 0) {
    const duplicateReminder = findDuplicity(REMINDERS, r)

    if (duplicateReminder) {
      return { status: 'error', message: 'Duplicate reminder name' }
    }

    REMINDERS[editedReminderIndex] = r
    return { status: 'ok' }
  }
  return { status: 'error', message: 'The reminder does not exist' }
}

export const remove = (id) => {
  const deletedReminderIndex = REMINDERS.findIndex(
    (reminder) => reminder.id === id
  )

  if (deletedReminderIndex >= 0) {
    REMINDERS.splice(deletedReminderIndex, 1)
    return true
  }
}

export const removeAll = () => {
  REMINDERS.splice(0, REMINDERS.length)
  return true
}
