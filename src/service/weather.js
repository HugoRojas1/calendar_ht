import axios from 'axios'

export const getWeatherByCity = async (city) => {
  const response = await axios.get(
    `${process.env.REACT_APP_API_WEATHER_URL}?q=${city}&appid=${process.env.REACT_APP_API_KEY_WEATHER}`
  )
  if (response.data && response.data.weather && response.data.weather.length) {
    return { status: 'ok', data: response.data.weather[0].main }
  } else {
    return { status: 'error', message: 'unable to connect to weather service' }
  }
}
