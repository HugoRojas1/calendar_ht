import { createContext } from 'react'

const ReminderContext = createContext({
  reminders: [],
})

export default ReminderContext
